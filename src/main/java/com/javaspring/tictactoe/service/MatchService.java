package com.javaspring.tictactoe.service;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.javaspring.tictactoe.exception.InvalidParamException;
import com.javaspring.tictactoe.exception.MatchNotFoundException;
import com.javaspring.tictactoe.model.Match;
import com.javaspring.tictactoe.model.MatchPlay;
import com.javaspring.tictactoe.model.Player;
import com.javaspring.tictactoe.model.Status;
import com.javaspring.tictactoe.model.Tictac;
import com.javaspring.tictactoe.storage.Storage;

@Service
public class MatchService {
	
    public Match createMatch(Player player) {
    	Match match = new Match();
    	match.setBoard(new int[3][3]);
    	match.setMatchId(UUID.randomUUID().toString());
    	match.setPlayer1(player);
    	match.setStatus(Status.NEW);
    	match.setTurn(Tictac.X);
        Storage.getInstance().setMatch(match);
        return match;
    }

    public Match connectToMatch(Player player2, String matchId) throws MatchNotFoundException, InvalidParamException {
        if (!Storage.getInstance().getMatchs().containsKey(matchId)) {
            throw new MatchNotFoundException("Match not found with the id: " + matchId);
        }
        Match match = Storage.getInstance().getMatchs().get(matchId);

        if (match.getPlayer2() != null) {
            throw new InvalidParamException("Match is not valid anymore");
        }

        match.setPlayer2(player2);
        match.setStatus(Status.IN_PROGRESS);
        Storage.getInstance().setMatch(match);
        return match;
    }

    public Match matchPlay(MatchPlay matchPlay) throws MatchNotFoundException {
        if (!Storage.getInstance().getMatchs().containsKey(matchPlay.getMatchId())) {
            throw new MatchNotFoundException("Match not found with the id: " + matchPlay.getMatchId());
        }

        Match match = Storage.getInstance().getMatchs().get(matchPlay.getMatchId());
        if (match.getStatus().equals(Status.FINISHED)) {
            throw new InvalidParamException("Match is already finished");
        }
        
        if (match.getPlayer2() == null) {
        	throw new InvalidParamException("The match is not valid, waiting for an opponent");
        }
        
        if (!match.getTurn().equals(matchPlay.getType())) {
        	throw new InvalidParamException("It's " + match.getTurn() + "'s turn");
        }

        int[][] board = match.getBoard();
        int boardSpot = board[matchPlay.getCoordinateX()][matchPlay.getCoordinateY()];
        
        if (boardSpot != 0) {
        	throw new InvalidParamException("You cannot play on a played position");
        }
        
        board[matchPlay.getCoordinateX()][matchPlay.getCoordinateY()] = matchPlay.getType().getValue();

        if (checkWinner(match.getBoard(), Tictac.X)) {
            match.setWinner(match.getPlayer1().getLogin());
            match.setStatus(Status.FINISHED);
        } else if (checkWinner(match.getBoard(), Tictac.O)) {
            match.setWinner(match.getPlayer2().getLogin());
            match.setStatus(Status.FINISHED);
        } else if (isDraw(match.getBoard())) {
        	match.setWinner("Draw");
            match.setStatus(Status.FINISHED);
        }
        
        match.setTurn(match.getTurn().equals(Tictac.X) ? Tictac.O : Tictac.X);

        Storage.getInstance().setMatch(match);
        return match;
    }

    private boolean checkWinner(int[][] board, Tictac tictac) {
        int[] boardArray = new int[9];
        int counterIndex = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boardArray[counterIndex] = board[i][j];
                counterIndex++;
            }
        }

        int[][] winCombinations = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};
        for (int i = 0; i < winCombinations.length; i++) {
            int counter = 0;
            for (int j = 0; j < winCombinations[i].length; j++) {
                if (boardArray[winCombinations[i][j]] == tictac.getValue()) {
                    counter++;
                    if (counter == 3) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
	public boolean isDraw(int[][] board) {
		for(int r = 0 ;  r < 3;  ++r) {
			for(int c = 0 ;  c < 3;  ++c) {
				if(board[r][c] == 0) {
					return false;
				}
			}
		}
		return true;
	}
}
