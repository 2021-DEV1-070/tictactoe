package com.javaspring.tictactoe.model;

public class ConnectRequest {
	
	private Player player;
    private String matchId;
    
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

}
