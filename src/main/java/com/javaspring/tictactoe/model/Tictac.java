package com.javaspring.tictactoe.model;

public enum Tictac {
	X(1), O(2);
	
	private int value;
	
	Tictac(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
