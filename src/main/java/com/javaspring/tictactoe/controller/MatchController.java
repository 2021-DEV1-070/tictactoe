package com.javaspring.tictactoe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javaspring.tictactoe.exception.InvalidParamException;
import com.javaspring.tictactoe.exception.MatchNotFoundException;
import com.javaspring.tictactoe.model.ConnectRequest;
import com.javaspring.tictactoe.model.Match;
import com.javaspring.tictactoe.model.MatchPlay;
import com.javaspring.tictactoe.model.Player;
import com.javaspring.tictactoe.service.MatchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/v1")
@Api(tags = "Tic Tac Toe Match")
public class MatchController {

	@Autowired
	private MatchService matchService;

	@PostMapping("/newMatch")
	@ApiOperation(value = "Start a new match")
	public ResponseEntity<Match> start(@ApiParam(value = "To start a match you just need to set your login", required = true)@RequestBody Player player) {
		return ResponseEntity.ok(matchService.createMatch(player));
	}

	@PostMapping("/connectMatch")
	@ApiOperation(value = "Connect to a match")
	public ResponseEntity<Match> connect(
			@ApiParam(value = "To connect to a match you must first have the match ID and set your login", required = true) @RequestBody ConnectRequest request)
			throws InvalidParamException {
		return ResponseEntity.ok(matchService.connectToMatch(request.getPlayer(), request.getMatchId()));
	}

	@PostMapping("/matchPlay")
	@ApiOperation(value = "Make a move")
	public ResponseEntity<Match> matchPlay(
			@ApiParam(value = "To make a move, you must first have the match ID, then choose the coordinates. "
					+ "\n You must choose a valid type between X and O and valid positions between 0 and 2", required = true) @RequestBody MatchPlay request)
			throws MatchNotFoundException {
		Match match = matchService.matchPlay(request);
		return ResponseEntity.ok(match);
	}

}
