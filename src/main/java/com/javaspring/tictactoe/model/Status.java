package com.javaspring.tictactoe.model;

public enum Status {
	NEW, IN_PROGRESS, FINISHED
}
