package com.javaspring.tictactoe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MatchNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MatchNotFoundException(String exception) {
		super(exception);
	}
}
