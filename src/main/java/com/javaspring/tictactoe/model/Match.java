package com.javaspring.tictactoe.model;

public class Match {
	
	private String matchId;
    private Player player1;
    private Player player2;
    private Status status;
    private int[][] board;
    private String winner;
    private Tictac turn;
    
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
	public Player getPlayer1() {
		return player1;
	}
	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}
	public Player getPlayer2() {
		return player2;
	}
	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public int[][] getBoard() {
		return board;
	}
	public void setBoard(int[][] board) {
		this.board = board;
	}
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winner) {
		this.winner = winner;
	}
	public Tictac getTurn() {
		return turn;
	}
	public void setTurn(Tictac turn) {
		this.turn = turn;
	}

}
