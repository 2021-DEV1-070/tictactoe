package com.javaspring.tictactoe;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.javaspring.tictactoe.model.ConnectRequest;
import com.javaspring.tictactoe.model.Match;
import com.javaspring.tictactoe.model.MatchPlay;
import com.javaspring.tictactoe.model.Player;
import com.javaspring.tictactoe.model.Status;
import com.javaspring.tictactoe.model.Tictac;

import reactor.core.publisher.Mono;

@SpringBootTest
class TictactoeApplicationTests {

	@Autowired
	private WebClient webClient;


	@Test
	void startNewMatchTest() {
		
		Player player1 = new Player();
		player1.setLogin("Kasparov");

		Mono<Match> monoMatch = this.webClient.post()
				.uri("/newMatch")
				.body(BodyInserters.fromValue(player1))
				.retrieve()
				.bodyToMono(Match.class);

		Match match = monoMatch.block();

		assertEquals(match.getPlayer1().getLogin(), player1.getLogin());
		assertEquals(Status.NEW, match.getStatus());
		assertNotNull(match.getBoard());
		assertNull(match.getPlayer2());
		assertNotNull(match.getMatchId());
		assertEquals(Tictac.X, match.getTurn());
		assertNull(match.getWinner());

	}
	
	@Test
	void connectToMatchTest() {
		
		Player player1 = new Player();
		player1.setLogin("Kasparov");

		Mono<Match> monoMatch = this.webClient.post()
				.uri("/newMatch")
				.body(BodyInserters.fromValue(player1))
				.retrieve()
				.bodyToMono(Match.class);

		Match match = monoMatch.block();
		
		ConnectRequest connectRequest = new ConnectRequest();
		Player player2 = new Player();
		player2.setLogin("Karpov");
		connectRequest.setPlayer(player2);
		connectRequest.setMatchId(match.getMatchId());
		
		Mono<Match> monoMatchConnect = this.webClient.post()
				.uri("/connectMatch")
				.body(BodyInserters.fromValue(connectRequest))
				.retrieve()
				.bodyToMono(Match.class);

		Match matchConnect = monoMatchConnect.block();
		
		assertEquals(matchConnect.getPlayer2().getLogin(), player2.getLogin());
		assertEquals(Status.IN_PROGRESS, matchConnect.getStatus());
		assertNotNull(matchConnect.getBoard());
		assertEquals(Tictac.X, match.getTurn());
		assertNull(matchConnect.getWinner());	
	}
	
	@Test
	void matchPlayTest() {
		
		Player player1 = new Player();
		player1.setLogin("Kasparov");

		Mono<Match> monoMatch = this.webClient.post()
				.uri("/newMatch")
				.body(BodyInserters.fromValue(player1))
				.retrieve()
				.bodyToMono(Match.class);

		Match match = monoMatch.block();
		
		ConnectRequest connectRequest = new ConnectRequest();
		Player player2 = new Player();
		player2.setLogin("Karpov");
		connectRequest.setPlayer(player2);
		connectRequest.setMatchId(match.getMatchId());
		
		Mono<Match> monoMatchConnect = this.webClient.post()
				.uri("/connectMatch")
				.body(BodyInserters.fromValue(connectRequest))
				.retrieve()
				.bodyToMono(Match.class);
		
		Match matchConnect = monoMatchConnect.block();
		
		List<MatchPlay> matchPlayList = loadMatchPlayMoves(matchConnect.getMatchId());
		
		Match matchPlayWinner = null;
		
		for (MatchPlay matchPlay : matchPlayList) {
			
			Mono<Match> monoMatchPlayWinner = this.webClient.post()
			.uri("/matchPlay")
			.body(BodyInserters.fromValue(matchPlay))
			.retrieve()
			.bodyToMono(Match.class);	
			
			matchPlayWinner = monoMatchPlayWinner.block();
		}

		assertEquals(matchPlayWinner.getWinner(), player1.getLogin());
		assertEquals(Status.FINISHED, matchPlayWinner.getStatus());
	}
	
	
	List<MatchPlay> loadMatchPlayMoves (String matchId) {
		
		MatchPlay matchPlay1 = new MatchPlay();
		matchPlay1.setType(Tictac.X);
		matchPlay1.setCoordinateX(0);
		matchPlay1.setCoordinateY(0);
		matchPlay1.setMatchId(matchId);
		
		MatchPlay matchPlay2 = new MatchPlay();
		matchPlay2.setType(Tictac.O);
		matchPlay2.setCoordinateX(1);
		matchPlay2.setCoordinateY(0);
		matchPlay2.setMatchId(matchId);
		
		MatchPlay matchPlay3 = new MatchPlay();
		matchPlay3.setType(Tictac.X);
		matchPlay3.setCoordinateX(0);
		matchPlay3.setCoordinateY(1);
		matchPlay3.setMatchId(matchId);
		
		MatchPlay matchPlay4 = new MatchPlay();
		matchPlay4.setType(Tictac.O);
		matchPlay4.setCoordinateX(1);
		matchPlay4.setCoordinateY(1);
		matchPlay4.setMatchId(matchId);
		
		MatchPlay matchPlay5 = new MatchPlay();
		matchPlay5.setType(Tictac.X);
		matchPlay5.setCoordinateX(0);
		matchPlay5.setCoordinateY(2);
		matchPlay5.setMatchId(matchId);
		
		List<MatchPlay> matchPlayList = new ArrayList<MatchPlay>();
		
		matchPlayList.add(matchPlay1);
		matchPlayList.add(matchPlay2);
		matchPlayList.add(matchPlay3);
		matchPlayList.add(matchPlay4);
		matchPlayList.add(matchPlay5);
		
		return matchPlayList;
	}
}
