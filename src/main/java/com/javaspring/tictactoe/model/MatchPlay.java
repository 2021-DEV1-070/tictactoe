package com.javaspring.tictactoe.model;

public class MatchPlay {
	
    private Tictac type;
    private Integer coordinateX;
    private Integer coordinateY;
    private String matchId;
    
	public Tictac getType() {
		return type;
	}
	public void setType(Tictac type) {
		this.type = type;
	}
	public Integer getCoordinateX() {
		return coordinateX;
	}
	public void setCoordinateX(Integer coordinateX) {
		this.coordinateX = coordinateX;
	}
	public Integer getCoordinateY() {
		return coordinateY;
	}
	public void setCoordinateY(Integer coordinateY) {
		this.coordinateY = coordinateY;
	}
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
}
