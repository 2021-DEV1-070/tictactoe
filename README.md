# Tic Tac Toe
Spring Boot Tic Tac Toe

# Technologies
Java 8  
Spring Boot  
Swagger UI  

# How to run
To run the application you only need to start Spring Boot (Server Port: 8080) and access the link below to access the Tic Tac Toe endpoints: 
[Swagger UI](http://localhost:8080/swagger-ui.html)
