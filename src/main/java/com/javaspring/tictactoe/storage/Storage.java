package com.javaspring.tictactoe.storage;

import java.util.HashMap;
import java.util.Map;

import com.javaspring.tictactoe.model.Match;

public class Storage {
	
    private static Map<String, Match> matchs;
    private static Storage instance;

    private Storage() {
    	matchs = new HashMap<>();
    }

    public static synchronized Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    public Map<String, Match> getMatchs() {
        return matchs;
    }

    public void setMatch(Match match) {
    	matchs.put(match.getMatchId(), match);
    }

}
