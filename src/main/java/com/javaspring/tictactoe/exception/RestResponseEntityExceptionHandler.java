package com.javaspring.tictactoe.exception;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 

	  @ExceptionHandler(MatchNotFoundException.class)
	  public final ResponseEntity<ExceptionResponse> handleMatchNotFoundException(MatchNotFoundException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(HttpStatus.NOT_FOUND.value(), dateFormat.format(new Date()), ex.getMessage());
	    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
	  }
	  
	  @ExceptionHandler(InvalidParamException.class)
	  public final ResponseEntity<ExceptionResponse> handleInvalidParamException(InvalidParamException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), dateFormat.format(new Date()), ex.getMessage());
	    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	  }
	  
	  @ExceptionHandler(ArrayIndexOutOfBoundsException.class)
	  public final ResponseEntity<ExceptionResponse> handleArrayIndexOutOfBoundsException(ArrayIndexOutOfBoundsException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(), dateFormat.format(new Date()), "Choose valid positions.");
	    return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	  }
}
